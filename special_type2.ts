let w: unknown = 1;
w = "string";
w = {
    runANonExistentMethod: () => {
        console.log("Hello M***ker");
    }
} as { runANonExistentMethod: () => void } 

if(typeof w === 'object' && w !== null){
    (w as { runANonExistentMethod: Function }).runANonExistentMethod();
}