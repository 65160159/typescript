function getTime(): number {
    return new Date().getTime();
}

console.log(getTime());

function prinHelloWorld() {
    console.log("Hello World");
}

prinHelloWorld();

function multiply(a: number,b:number): number{
    return a*b;
}

console.log(multiply(1,2));

function addNum(a:number, b:number, c?:number){
    return a + b + ( c || 0);
}

console.log(addNum(1,2,3));

function pow(val: number, expo: number =10){
    return val ** expo;
}

console.log(pow(10));
console.log(pow(10,2));

function divide({dividend, divisor}: { dividend: number, divisor: number}){
    return dividend / divisor;
}

console.log(divide({dividend: 100, divisor: 10}));


//rest
function add2(a: number, b:number, ...rest: number[]){
    return a + b + rest.reduce((p,c) => p+c, 0);
}

console.log(add2(1,2,10,10,10));


//Negate
type Negate = (value: number) => number;

const negateFunction: Negate = (value: number) => value * -1;
const negateFunction2: Negate = function(value:number): number{ 
    return value * -1;
};
console.log(negateFunction(1));
console.log(negateFunction2(1));