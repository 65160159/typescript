function printStatusCode(code: string|number){
    if(typeof code === 'string'){
        console.log(`My status code is ${code.toUpperCase()} type ${typeof code}`); //ต้องใช้ `
    }else {
        console.log(`My status code is ${code} type ${typeof code}`); //ต้องใช้ `
    }
}
printStatusCode(404);
printStatusCode("abc");